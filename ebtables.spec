
Name:             ebtables
Version:          2.0.11
Release:          12
Summary:          A filtering tool for a Linux-based bridging firewall
License:          GPLv2+
URL:              http://ebtables.sourceforge.net/
Source0:          http://ftp.netfilter.org/pub/ebtables/ebtables-%{version}.tar.gz
Source2:          ebtables.systemd
Source3:          ebtables.service
Source4:          ebtables-config

Patch0001:        bugifx-Fix-string-case-option-can-not-work.patch   
Patch0002:        bugfix-Fix-ebt_print_error-for-prints-null.patch

BuildRequires:    systemd libtool
BuildRequires:    chrpath
Requires:         systemd %{_sbindir}/update-alternatives
Conflicts:        setup < 2.10.4-1

%description
The ebtables program is a filtering tool for a Linux-based bridging firewall.It enables transparent filtering of network traffic passing through a Linux bridge.The filtering possibilities are limited to link layer filtering and some basic filtering on higher network layers.
The ebtables tool can be combined with the other Linux filtering tools to make a bridging firewall that is also capable of filtering these higher network layers. This is enabled through the bridge-netfilter architecture which is a part of the standard Linux kernel.

%package help
Summary: help documents for ebtables

%description help
Help package contains some doc and man help files for ebtables.

%prep
%autosetup -n %{name}-%{version} -p1
f=THANKS; iconv -f iso-8859-1 -t utf-8 $f -o $f.utf8 ; mv $f.utf8 $f

%build
./autogen.sh
%ifarch sw_64
./configure --mandir=/usr/share/man --bindir=/usr/sbin --sbindir=/usr/sbin --libdir=/lib/ebtables --sysconfdir=/etc
%else
./configure --mandir=/usr/share/man --bindir=/usr/sbin --sbindir=/usr/sbin --libdir=/lib64/ebtables --sysconfdir=/etc
%endif
%make_build CFLAGS="${RPM_OPT_FLAGS}" LIBDIR="/%{_lib}/ebtables" BINDIR="%{_sbindir}" MANDIR="%{_mandir}" LDFLAGS="${RPM_LD_FLAGS} -Wl,-z,now"

%install
install -d %{buildroot}{%{_initrddir},%{_unitdir},%{_libexecdir},%{_sysconfdir}/sysconfig}
install -p %{SOURCE3} %{buildroot}%{_unitdir}/
install -p -D -m 600 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/ebtables-config
chmod -x %{buildroot}%{_unitdir}/*.service
install -m0755 %{SOURCE2} %{buildroot}%{_libexecdir}/ebtables
%make_install LIBDIR="/%{_lib}/ebtables" BINDIR="%{_sbindir}" MANDIR="%{_mandir}"
touch %{buildroot}%{_sysconfdir}/sysconfig/{ebtables.nat,ebtables.filter,ebtables.broute}


# remove rpath or runpath
chrpath -d %{buildroot}%{_sbindir}/ebtablesd
chrpath -d %{buildroot}%{_sbindir}/ebtables-legacy
chrpath -d %{buildroot}%{_sbindir}/ebtables-legacy-restore

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/ebtables" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%post
%systemd_post ebtables.service
/usr/sbin/ldconfig
pfx=%{_sbindir}/ebtables
manpfx=%{_mandir}/man8/ebtables
for sfx in "" "-restore" "-save"; do
	if [ "$(readlink -e $pfx$sfx)" == $pfx$sfx ]; then
		rm -f $pfx$sfx
	fi
	if [ -e /var/lib/alternatives/ebtables$sfx ]; then
		rm -f /var/lib/alternatives/ebtables$sfx
	fi
done
if [ "$(readlink -e $manpfx.8.gz)" == $manpfx.8.gz ]; then
	rm -f $manpfx.8.gz
fi
%{_sbindir}/update-alternatives --install \
	$pfx ebtables $pfx-legacy 10 \
	--slave $pfx-save ebtables-save $pfx-legacy-save \
	--slave $pfx-restore ebtables-restore $pfx-legacy-restore \
	--slave $manpfx.8.gz ebtables-man $manpfx-legacy.8.gz

%preun
%systemd_preun ebtables.service

%postun
%systemd_postun_with_restart ebtables.service
/usr/sbin/ldconfig
if [ $1 -eq 0 ]; then
	%{_sbindir}/update-alternatives --remove \
		ebtables %{_sbindir}/ebtables-legacy
    if [ -e /usr/sbin/iptables-nft ]; then
        pfx=%{_sbindir}/ebtables
        manpfx=%{_mandir}/man8/ebtables
        %{_sbindir}/update-alternatives --install \
        $pfx ebtables $pfx-nft 10 \
        --slave $pfx-save ebtables-save $pfx-nft-save \
        --slave $pfx-restore ebtables-restore $pfx-nft-restore \
        --slave $manpfx.8.gz ebtables-man $manpfx-nft.8.gz
	systemctl status firewalld 2>/dev/null | grep -w "running" > /dev/null
	if [ $? -eq 0 ];then
		systemctl restart firewalld || :
	fi
    fi
fi

%files
%license COPYING
%{_unitdir}/ebtables.service
%{_libexecdir}/ebtables
/%{_lib}/ebtables/
%{_sbindir}/ebtables-*
%{_sbindir}/ebtables*
%exclude %{_initrddir}
%exclude %{_sysconfdir}/ethertypes
%config(noreplace) %{_sysconfdir}/sysconfig/ebtables-config
%config(noreplace) /etc/ld.so.conf.d/*
%ghost %{_sbindir}/ebtables
%ghost %{_sbindir}/ebtables-restore
%ghost %{_sbindir}/ebtables-save
%ghost %{_sysconfdir}/sysconfig/{ebtables.filter,ebtables.nat,ebtables.broute}

%files help
%doc ChangeLog THANKS
%doc %{_mandir}/man8/ebtables-legacy.8*

%changelog
* Tue Jul 30 2024 xinghe <xinghe2@h-partners.com> - 2.0.11-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix ebtables-restore --noflush failed when ebtables is uninstall

* Wed Sep 20 2023 xinghe <xinghe2@h-partners.com> - 2.0.11-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add release

* Thu Aug 31 2023 lifeifei<lifeifei@kylinos.cn> - 2.0.11-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix: if libvirt and ebtables are installed at the same time, ping the operation prompts "sendmsg: Operation not permitted".

* Mon Aug 07 2023 gaihuiying <eaglegai@163.com> - 2.0.11-9
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix post error "No such file or directory"

* Fri Jun 30 2023 xinghe <xinghe2@h-partners.com> - 2.0.11-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix install error

* Mon Nov 28 2022 yanglu<yanglu72@h-partners.com> - 2.0.11-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:replace with tar from source0

* Thu Nov 3 2022 wuzx<wuzx1226@qq.com> - 2.0.11-6
- Type:feature
- CVE:NA
- SUG:NA
- DESC:change lib64 to lib when in sw64 architecture

* Mon Jul 04 2022 gaihuiying <eaglegai@163.com> - 2.0.11-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix ebtables-config file access permisson
       fix ebt_print_error for prints null

* Thu Mar 24 2022 wangxiaopeng <wangxiaopeng7@huawei.com> - 2.0.11-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:del broken lib

* Tue Mar 09 2022 wangxiaopeng <wangxiaopeng7@huawei.com> - 2.0.11-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix string-icase can not work

* Tue Sep 07 2021 gaihuiying <gaihuiying1@huawei.com> - 2.0.11-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove runpath of ebtables

* Mon Jul 27 2020 openEuler Buildteam <buildteam@huawei.com> - 2.0.11-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Upgrade to version 2.0.11

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.10-32
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build fail for firewalld

* Tue Jan 14 2020 zhangrui  <zhangrui182@huawei.com> - 2.0.10-31
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build fail for firewalld

* Thu Sep 24 2019 gaoguanghui <gaoguanghui1@huawei.com> - 2.0.10-30
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:this patch made future ebtables processes wait indefinitely for the lock to
       become free

* Mon Sep 16 2019 yanzhihua <yanzhihua4@huawei.com> - 2.0.10-29
- Package init

